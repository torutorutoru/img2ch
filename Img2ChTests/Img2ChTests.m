//
//  Img2ChTests.m
//  Img2ChTests
//
//  Created by Toru Hisai on 12/08/14.
//  Copyright (c) 2012年 Kronecker's Delta Studio. All rights reserved.
//

#import "Img2ChTests.h"
#import "lualib.h"
#import "lauxlib.h"

@interface TestFunctions : NSObject
- (int) pointerToInt:(void*)ptr;
@end

@implementation TestFunctions

- (int) pointerToInt:(void*)ptr
{
    return (int)ptr;
}

+ (void*) getPointer
{
    return (void*)0xbeef;
}
@end

@implementation Img2ChTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
    brdg = [LuaBridge instance];
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

//- (void)testExample
//{
//    STFail(@"Unit tests are not implemented yet in Img2ChTests");
//}
- (void)testUIWebView
{
//    UIWebView *wv = [UIWebView alloc];
//    [wv initWithFrame:CGRectMake(0, 20, 200, 200)];
    
    UIWebView *wv = [UIWebView new];
    [wv release]; // OK
    
//    UIWebView *wv2 = [UIWebView alloc];
//    [wv2 retain]; // EXC_BAD_ACCESS
}
- (void)testNewStack
{
    lua_State *L = [brdg L];
    int n = luafunc_newstack(L);
    STAssertEquals(n, 1, @"new stack");
    
    NSMutableArray *arr = (NSMutableArray*)lua_topointer(L, -1);
    STAssertEquals([arr isKindOfClass:[NSMutableArray class]], YES, @"stack is NSMutableArray");
}

- (void)script:(NSString*)str loadAndRunAndGetReturn:(int)num
{
    const char *code = [str UTF8String];
    lua_State *L = [brdg L];
    
    int ret_ls = luaL_loadstring(L, code);
    STAssertEquals(ret_ls, LUA_OK, @"loadstring");
    if (ret_ls) {
        NSLog(@"Lua Error: %s", lua_tostring(L, -1));
    }
    
    int ret_run = lua_pcall(L, 0, num, 0);
    if (ret_run) {
        NSLog(@"Lua Error: %s", lua_tostring(L, -1));
    }
    STAssertEquals(ret_run, LUA_OK, @"run");
}

- (void)testPointer
{
    lua_State *L = [brdg L];
    
    TestFunctions *t = [[TestFunctions alloc] init];
    void *ptr = (void*)"hogehoge";
    int ret = [t pointerToInt:ptr];
    STAssertEquals(ptr, (void*)ret, @"pointer");
    
    NSString* code =
    @"local stack = objc.newstack();"
    "local TestFunctions = objc.getclass 'TestFunctions';"
    
    "objc.push(stack, TestFunctions, 'getPointer');"
    "print('TestFunctions', TestFunctions);"
    "objc.operate(stack, 'call');"
    "local ptr = objc.pop(stack);"
    "print('ptr', ptr);"

    "objc.push(stack, TestFunctions, 'alloc');"
    "objc.operate(stack, 'call');"
    "local obj = objc.pop(stack);"

    "objc.push(stack, ptr, obj, 'pointerToInt:');"
    "objc.operate(stack, 'call');"
    "local ret = objc.pop(stack);"
    "print(string.format('pointerToInt: %x', ret));"
    "return ret;";
    
    [self script:code loadAndRunAndGetReturn:1];

    STAssertEquals(lua_tointeger(L, -1), 0xbeef, @"correct pointer");
}

- (void)testCGRect
{
    lua_State *L = [brdg L];

    NSString * code =
    @"local stack = objc.newstack();"
    "objc.push(stack, 123, 456, 234, 345);"
    "objc.operate(stack, 'cgrectmake');"
    "local rect = objc.pop(stack);"
    "print(rect);"
    "return rect";

    [self script:code loadAndRunAndGetReturn:1];
    
    void *ptr = lua_touserdata(L, -1);
    NSValue *valptr = (NSValue *)*(void**)ptr;
    CGRect rect = [valptr CGRectValue];
    
    NSLog(@"CGRect = %@", NSStringFromCGRect(rect));
    STAssertEquals((int)rect.origin.x, 345, @"x");
    STAssertEquals((int)rect.origin.y, 234, @"y");
    STAssertEquals((int)rect.size.width, 456, @"width");
    STAssertEquals((int)rect.size.height, 123, @"height");
}

- (void)testUsingCGRect
{
    lua_State *L = [brdg L];
    
    NSString * code =
    @"local stack = objc.newstack();"
    "local UIView = objc.getclass 'UIView';"

    "objc.push(stack, UIView, 'alloc');"
    "objc.operate(stack, 'call')"
    "local v = objc.pop(stack);"

    "objc.push(stack, 123, 456, 234, 345);"
    "objc.operate(stack, 'cgrectmake');"
    "local rect = objc.pop(stack);"

    "objc.push(stack, rect, v, 'initWithFrame:');"
    "objc.operate(stack, 'call');"

    "local v2 = objc.pop(stack);"
    "return v2;";

    [self script:code loadAndRunAndGetReturn:1];

    void *ptr = lua_touserdata(L, -1);
    id valptr = (id)*(void**)ptr;
    
    NSLog(@"UIView = %@", valptr);
}

- (void)testUsingCGAffineTransform
{
    lua_State *L = [brdg L];
    
    NSString * code =
    @"local tran = cg.CGAffineTransformMake(1, 2, 3, 4, 5, 6);"
    "return cg.CGAffineTransformWrap(tran);"
    ;
    
    [self script:code loadAndRunAndGetReturn:1];
    
    void *ptr = lua_touserdata(L, -1);
    id valptr = (id)*(void**)ptr;
    
    NSLog(@"UIValue = %@", valptr);
}

@end
