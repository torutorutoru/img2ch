//
//  KDViewController.m
//  Img2Ch
//
//  Created by Toru Hisai on 12/08/14.
//  Copyright (c) 2012年 Kronecker's Delta Studio. All rights reserved.
//

#import "KDViewController.h"
#import "LuaBridge.h"
#import "lualib.h"
#import "lauxlib.h"

@interface KDViewController ()

@end

@implementation KDViewController

@synthesize stackRef;

void luaopen_cg(lua_State* L);

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    LuaBridge *brdg = [LuaBridge instance];
    lua_State *L = [brdg L];
    luaopen_cg(L);
    NSString *path = [[NSBundle mainBundle] pathForResource:@"bootstrap" ofType:@"lua"];
    if (!path) {
        NSLog(@"Lua Error: file not found");
    }
    int result = luaL_loadfile(L, [path UTF8String]);
    
    if (result != LUA_OK) {
        NSLog(@"Lua Error: load error: %d: %s", result, lua_tostring(L, -1));
    } else {
        int result = lua_pcall(L, 0, 0, 0);
        if (result) {
            NSLog(@"Lua Error: %s", lua_tostring(L, -1));
        }
    }
    
    lua_getglobal(L, "init");
    [brdg pushObject:self];

    if (lua_pcall(L, 1, 1, 0)) {
        NSLog(@"Lua Error: %s", lua_tostring(L, -1));
    } else {
        stackRef = luaL_ref(L, LUA_REGISTRYINDEX);
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end
