//
//  NSData+NSData_SHA1.m
//  Img2Ch
//
//  Created by Toru Hisai on 12/11/18.
//  Copyright (c) 2012年 Kronecker's Delta Studio. All rights reserved.
//

#import "NSData+NSData_SHA1.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSData (NSData_SHA1)
- (NSData*)digestSHA1
{
    NSMutableData *hashData = [NSMutableData dataWithLength:CC_SHA1_DIGEST_LENGTH];
    CC_SHA1([self bytes], [self length], [hashData mutableBytes]);
    
    return hashData;
}
- (BOOL)writeToTemporaryDirectory:(NSString*)filename
{
    NSString *tmp = NSTemporaryDirectory();
    NSString *path = [NSString pathWithComponents:[NSArray arrayWithObjects:tmp, filename, nil]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSLog(@"%s, %@", __PRETTY_FUNCTION__, path);
        return YES;
    }
    return [self writeToFile:path options:0 error:nil];
}
+ (NSData*)dataWithContentOfTemporaryFile:(NSString*)filename
{
    NSString *tmp = NSTemporaryDirectory();
    NSString *path = [NSString pathWithComponents:[NSArray arrayWithObjects:tmp, filename, nil]];
    NSData *dat = [NSData dataWithContentsOfFile:path];
    return dat;
}
@end
