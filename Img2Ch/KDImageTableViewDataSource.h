//
//  KDImageTableViewDataSource.h
//  Img2Ch
//
//  Created by Toru Hisai on 12/11/11.
//  Copyright (c) 2012年 Kronecker's Delta Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LuaBridge.h"

@interface KDImageTableViewDataSource : NSObject
@property (nonatomic, retain) LuaObjectReference *contextRef;
@end
