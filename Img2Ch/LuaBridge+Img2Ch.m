//
//  LuaBridge+Img2Ch.m
//  Img2Ch
//
//  Created by Toru Hisai on 12/08/15.
//  Copyright (c) 2012年 Kronecker's Delta Studio. All rights reserved.
//

#import "LuaBridge.h"
#import "KDViewController.h"
#import "LuaBridge+Img2Ch.h"
#import "KDImageTableViewDelegate.h"

@implementation LuaBridge (Img2Ch)
- (void)op_make_gesture_recognizer:(NSMutableArray*)stack
{
    UITapGestureRecognizer *ges = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)] autorelease];

    NSMutableArray *arr = (NSMutableArray*)lua_topointer(L, 1);
    UIView *view = (UIView*)[[[arr lastObject] retain] autorelease];
    [arr removeLastObject];
    
    [view addGestureRecognizer:ges];
}

- (void)handleTapGesture:(UIGestureRecognizer *)sender
{
    lua_getglobal(L, "handle_tap");
    [self pushObject:sender];
    if (lua_pcall(L, 1, 0, 0)) {
        NSLog(@"Lua Error: %s", lua_tostring(L, -1));
    }
}
@end
