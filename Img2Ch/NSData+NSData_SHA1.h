//
//  NSData+NSData_SHA1.h
//  Img2Ch
//
//  Created by Toru Hisai on 12/11/18.
//  Copyright (c) 2012年 Kronecker's Delta Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (NSData_SHA1)
- (NSData*)digestSHA1;
- (BOOL)writeToTemporaryDirectory:(NSString*)filename;
+ (NSData*)dataWithContentOfTemporaryFile:(NSString*)filename;
@end
