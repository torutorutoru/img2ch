//
//  LuaBridge+Img2Ch.h
//  Img2Ch
//
//  Created by Toru Hisai on 12/08/15.
//  Copyright (c) 2012年 Kronecker's Delta Studio. All rights reserved.
//

#import "LuaBridge.h"

@interface LuaBridge (Img2Ch)
//- (void)op_make_webview:(NSMutableArray*)stack;
- (void)op_make_gesture_recognizer:(NSMutableArray*)stack;
- (void)handleTapGesture:(UIGestureRecognizer *)sender;
@end
