//
//  KDImageTableViewDelegate.m
//  Img2Ch
//
//  Created by Toru Hisai on 12/10/24.
//  Copyright (c) 2012年 Kronecker's Delta Studio. All rights reserved.
//

#import "KDImageTableViewDelegate.h"
#import "LuaBridge.h"

@implementation KDImageTableViewDelegate
@synthesize contextRef;

- (void)dealloc
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [super dealloc];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    lua_State *L = [LuaBridge instance].L;
    lua_getglobal(L, "call_delegate");
    lua_pushstring(L, sel_getName(_cmd));
    lua_rawgeti(L, LUA_REGISTRYINDEX, self.contextRef.ref);

    luabridge_push_object(L, indexPath);
    
    if (lua_pcall(L, 3, 1, 0)) {
        NSString *msg = [NSString stringWithUTF8String: lua_tostring(L, -1)];
        NSLog(@"ERROR: %@", msg);
        [NSException raise:@"Lua Error" format:@"%@", msg];
        return -1;
    } else {
        CGFloat val = lua_tonumber(L, -1);
        return val;
    }
}
@end
