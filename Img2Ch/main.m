//
//  main.m
//  Img2Ch
//
//  Created by Toru Hisai on 12/08/14.
//  Copyright (c) 2012年 Kronecker's Delta Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KDAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KDAppDelegate class]));
    }
}
