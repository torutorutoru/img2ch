//
//  KDAppDelegate.h
//  Img2Ch
//
//  Created by Toru Hisai on 12/08/14.
//  Copyright (c) 2012年 Kronecker's Delta Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
