//
//  KDImageTableViewDataSource.m
//  Img2Ch
//
//  Created by Toru Hisai on 12/11/11.
//  Copyright (c) 2012年 Kronecker's Delta Studio. All rights reserved.
//

#import "KDImageTableViewDataSource.h"
#import "LuaBridge.h"

@implementation KDImageTableViewDataSource
@synthesize contextRef;

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    lua_State *L = [LuaBridge instance].L;
    lua_getglobal(L, "call_delegate");
    lua_pushstring(L, sel_getName(_cmd));
    lua_rawgeti(L, LUA_REGISTRYINDEX, self.contextRef.ref);
    lua_pushnumber(L, indexPath.section);
    lua_pushnumber(L, indexPath.row);

    if (lua_pcall(L, 4, 1, 0)) {
        NSString *msg = [NSString stringWithUTF8String: lua_tostring(L, -1)];
        NSLog(@"ERROR: %@", msg);
        [NSException raise:@"Lua Error" format:@"%@", msg];
        return nil;
    } else {
        void *p = lua_touserdata(L, -1);
        void **ptr = (void**)p;
        UITableViewCell *cell = *ptr;
        NSLog(@"%s: %@", __PRETTY_FUNCTION__, cell);
        return cell;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    lua_State *L = [LuaBridge instance].L;
    lua_getglobal(L, "call_delegate");
    lua_pushstring(L, sel_getName(_cmd));
    lua_rawgeti(L, LUA_REGISTRYINDEX, self.contextRef.ref);
    lua_pushnumber(L, section);
    
    if (lua_pcall(L, 3, 1, 0)) {
        NSString *msg = [NSString stringWithUTF8String: lua_tostring(L, -1)];
        NSLog(@"ERROR: %@", msg);
        [NSException raise:@"Lua Error" format:@"%@", msg];
        return -1;
    } else {
        NSInteger val = lua_tointeger(L, -1);
        return val;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    lua_State *L = [LuaBridge instance].L;
    lua_getglobal(L, "call_delegate");
    lua_pushstring(L, sel_getName(_cmd));
    lua_rawgeti(L, LUA_REGISTRYINDEX, self.contextRef.ref);
    
    if (lua_pcall(L, 2, 1, 0)) {
        NSString *msg = [NSString stringWithUTF8String: lua_tostring(L, -1)];
        NSLog(@"ERROR: %@", msg);
        [NSException raise:@"Lua Error" format:@"%@", msg];
        return -1;
    } else {
        NSInteger val = lua_tointeger(L, -1);
        return val;
    }
}
@end
