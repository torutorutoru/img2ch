//
//  KDImageTableViewDelegate.h
//  Img2Ch
//
//  Created by Toru Hisai on 12/10/24.
//  Copyright (c) 2012年 Kronecker's Delta Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LuaBridge.h"

@interface KDImageTableViewDelegate : NSObject <UITableViewDelegate>
@property (nonatomic, retain) LuaObjectReference *contextRef;
@end
