--*- coding: utf-8 -*-

setmetatable(_G, {__index = function (table, key)
                               error("undefined variable: " .. tostring(key))
                            end})

NSShiftJISStringEncoding = 8
NSUTF8StringEncoding = 4
UITableViewCellStyleDefault = 0
UITableViewStylePlain = 0

UIButtonTypeRoundedRect = 1

UIControlStateNormal               = 0
UIControlStateHighlighted          = 1
UIControlStateDisabled             = 2
UIControlStateSelected             = 4

UITableViewStylePlain = 0

-- --
-- -- Context
-- --

Context = {}
setmetatable(Context, {__index = objc.context})

function Context:loadImage (imgurl)
   if not self.image_table then self.image_table = {} end
   -- image_table: URL -> SHA1

   local imgbin

   if self.image_table[imgurl] then
      local hash = self.image_table[imgurl]
      imgbin = self:wrap(objc.class.NSData)("dataWithContentOfTemporaryFile:", hash)
      if imgbin then
         print("read from temporary file", imgurl, hash)
      end
   end

   if not imgbin then
      local nsurl = self:wrap(objc.class.NSURL)("URLWithString:", imgurl)
      imgbin = self:wrap(objc.class.NSData)("dataWithContentsOfURL:",
                                            -nsurl) or error "fail"

      local sha1 = imgbin("digestSHA1")("description")
      sha1 = sha1:gsub("[<>%s]", "")
      print("SHA1", sha1)
      self.image_table[imgurl] = sha1

      imgbin("writeToTemporaryDirectory:", sha1)
   end

   local img = self:wrap(objc.class.UIImage)("imageWithData:", -imgbin)

   return img
end

function Context:makeThumbnailView (img, thumbnail_size)
   local size = img("size")
   print("size", -size)
   objc.push(self.stack, -size)
   local w, h = objc.extract(self.stack, "CGSize")
   print ("w, h", w, h)

   local scale = thumbnail_size / math.max(w, h)
   print("scale", scale)
   local trans = cg.CGAffineTransformWrap(
      cg.CGAffineTransformMake(scale, 0, 0, scale,
                               -(w - thumbnail_size) / 2, -(h - thumbnail_size) / 2))

   local imgview = self:wrap(objc.class.UIImageView)("alloc")
   imgview("initWithImage:", -img)

   imgview("setTransform:", trans)

   return imgview
end

-- /Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS5.1.sdk/System/Library/Frameworks/UIKit.framework/Headers/UITableView.h

function Context:loadDat (view, dat_url, enc)
   print "Context:loadDat"
   local stat = self.stack
   local url = self:wrap(objc.class.NSURL)("URLWithString:", dat_url)
   local content = (self:wrap(objc.class.NSString)
                    ("stringWithContentsOfURL:encoding:error:", -url, enc, nil))
   local imgs = {}
   for url in content:gmatch "(http://[^%s]*%.jpe?g)" do
      table.insert(imgs, url)
      print(url)
   end

   return imgs
end

function Context:createView (view)
   local v = self:wrap(view)
   local tbl = (self:wrap(objc.class.UITableView)("alloc")
                ("initWithFrame:style:", -v("frame"),
                 UITableViewStylePlain))

   local delegate = self:wrap(objc.class.KDImageTableViewDelegate)("new")
   delegate("setContextRef:", self)

   local source = self:wrap(objc.class.KDImageTableViewDataSource)("new")
   source("setContextRef:", self)
   tbl("setDelegate:", -delegate)
   tbl("setDataSource:", -source)
   v("addSubview:", -tbl)

   self.table_view = {tbl, delegate, source}
end

function Context:setGestureHandler (view)
   objc.push(self.stack, view)
   objc.operate(self.stack, "make_gesture_recognizer")
end

function Context:addButtons (view)
   local btn_next = self:sendMesg(objc.class.UIButton,
                                  "buttonWithType:", UIButtonTypeRoundedRect)

   self:sendMesg(btn_next, "setTitle:forState:", "XXXXXXXXXXXXXX",
                 bit32.bor(UIControlStateNormal, UIControlStateHighlighted,
                           UIControlStateDisabled, UIControlStateSelected))

   self:sendMesg(view, "addSubview:", btn_next)

end

--
-- Delegate/DataSource
--

Delegate = {}

Delegate["tableView:heightForRowAtIndexPath:"] =
   function(stat, index_path)
      local ip = stat:wrap(index_path)
      local section = ip("section")
      local row = ip("row")
      print("delegate", stat, section, row)
      return 300
   end

Delegate["tableView:cellForRowAtIndexPath:"] =
   function(stat, section, row)
      print(stat, section, row)

      local imgurl = stat.image_urls[row + 1]
      local result, img = pcall(stat.loadImage, stat, imgurl)
      local cell = (stat:wrap(objc.class.UITableViewCell)("alloc")
                    ("initWithStyle:reuseIdentifier:",
                     UITableViewCellStyleDefault, "imageCell"))

      if result and img then
         local imgview = stat:makeThumbnailView(img, 300)
         cell("contentView")("addSubview:", -imgview)
      else
         cell("textLabel")("setText:", tostring(img) or "ERROR")
      end
      print("tableView:cellForRowAtIndexPath: ret:", -cell)

      return -cell
   end

Delegate["tableView:numberOfRowsInSection:"] =
   function(stat, section)
      print(stat)
      return #stat.image_urls
   end

Delegate["numberOfSectionsInTableView:"] =
   function(stat)
      print(stat)
      return 1
   end

--
-- callbacks
--
function init (viewctrl)
   local stat = Context:create()
   local view = stat:sendMesg(viewctrl, "view")

   print("stat:sendMesg", stat.sendMesg)
   print("stat:loadDat", stat.loadDat)

   local imgs = stat:loadDat(view, "http://engawa.2ch.net/dog/dat/1346191696.dat",
                             NSShiftJISStringEncoding)
   stat.image_urls = imgs
   stat:createView (view)

   stat:setGestureHandler(view)

   return stat
end

function handle_tap (event)
   print("gesture detected!", event)
end

function call_delegate (selector, ...)
   print("call_deletage", selector, ...)
   return Delegate[selector](...)
end
